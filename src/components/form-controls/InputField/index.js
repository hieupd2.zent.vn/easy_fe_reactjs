import React from 'react';
import PropTypes from 'prop-types';
import { TextField } from '@material-ui/core';
import { Controller } from 'react-hook-form';

function InputField(props) {
  const { form, name, label, disabled } = props;
  // lấy thông tin error khi touched: thông tin đầu vào mà người dùng đã tương tác và có lỗi
  const { errors } = form;
  const hasError = errors[name];

  return (
    <Controller
      // name và control bắt buộc phải có
      name={name}
      control={form.control}
      // as để xử dụng thư viện UI, render liên quan bọn on...
      // fullWidth và label được truyền vào TextField
      as={TextField}
      fullWidth
      label={label}
      disabled={disabled}
      variant='outlined'
      margin='normal'
      // hiển thị error
      error={!!hasError}
      helperText={errors[name]?.message}
    />
  );
}

InputField.propTypes = {
  form: PropTypes.object.isRequired,
  name: PropTypes.string.isRequired,

  label: PropTypes.string,
  disabled: PropTypes.bool,
};

export default InputField;
