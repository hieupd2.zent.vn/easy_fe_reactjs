import React, { useEffect, useMemo, useState } from 'react';
// import PropTypes from 'prop-types'
import { useHistory, useLocation, useRouteMatch } from 'react-router-dom/cjs/react-router-dom.min';
import queryString from 'query-string';
import TodoForm from 'features/Todo/components/TodoForm';
import TodoList from 'features/Todo/components/TodoList';

// component TodoList nhận dữ liệu là mảng todoList, nv TodoList là render ra mảng todoList
function ListPage(props) {
  const initTodoList = [
    {
      id: 1,
      title: 'Eat',
      status: 'new',
    },
    {
      id: 2,
      title: 'Sleep',
      status: 'completed',
    },
    {
      id: 3,
      title: 'Code',
      status: 'new',
    },
  ];

  const location = useLocation();
  const history = useHistory(); // để navigate đi
  const match = useRouteMatch(); // để nested routing
  const [todoList, setTodoList] = useState(initTodoList);
  const [filterStatus, setFilterStatus] = useState(() => {
    const params = queryString.parse(location.search);
    return params.status || 'all';
  });

  useEffect(() => {
    const params = queryString.parse(location.search);
    setFilterStatus(params.status || 'all');
  }, [location.search]);

  const handleTodoClick = (todo, idx) => {
    const newTodoList = [...todoList]; // clone array hiện tại thành 1 mảng mới

    console.log(todo, idx);
    // toggle state (cập nhật lại vị trí idx với all giá trị của nó và thay đổi stattus)
    // const newTodo = {
    newTodoList[idx] = {
      ...newTodoList[idx],
      status: newTodoList[idx].status === 'new' ? 'completed' : 'new',
    };
    // newTodoList[idx] = newTodo   // update lại vị trí tại idx

    // update todo list
    setTodoList(newTodoList);
  };

  const handleShowAllClick = () => {
    // setFilterStatus('all')
    const queryParams = { status: 'all' };
    history.push({
      pathname: match.path, // path hiện tại
      search: queryString.stringify(queryParams),
    });
  };

  const handleShowCompletedClick = () => {
    const queryParams = { status: 'completed' };
    history.push({
      pathname: match.path,
      search: queryString.stringify(queryParams),
    });
  };

  const handleShowNewClick = () => {
    const queryParams = { status: 'new' };
    history.push({
      pathname: match.path,
      search: queryString.stringify(queryParams),
    });
  };

  const renderedTodoList = useMemo(() => {
    return todoList.filter((todo) => filterStatus === 'all' || filterStatus === todo.status);
  }, [todoList, filterStatus]);

  const handleTodoFormSubmit = (values) => {
    console.log('Form submit:', values)
    const newTodo = {
      id: todoList.length + 1,
      title: values.title,
      status: 'new'
    }

    const newTodoList = [...todoList, newTodo]
    setTodoList(newTodoList)
  }

  return (
    <div>
      <h3>What to do</h3>
      <TodoForm onSubmit={handleTodoFormSubmit} />

      <h3>Todo List</h3>
      <TodoList todoList={renderedTodoList} onTodoClick={handleTodoClick} />

      <div>
        <button onClick={handleShowAllClick}>Show All</button>
        <button onClick={handleShowCompletedClick}>Show Completed</button>
        <button onClick={handleShowNewClick}>Show New</button>
      </div>
    </div>
  );
}

ListPage.propTypes = {};

export default ListPage;
