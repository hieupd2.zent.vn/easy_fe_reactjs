import React from 'react'
import PropTypes from 'prop-types'
import InputField from 'components/form-controls/InputField'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";

function TodoForm(props) {
  // validate vs yup
  const schema = yup.object().shape({
    title: yup.string()
      .required('Please enter title')
      .min(5, 'title is too short'),
  });

  const form = useForm({
    defaultValues: {
      title: ''
    },
    resolver: yupResolver(schema)
  })

  const handleSubmit = (values) => {
    // console.log('todo form: ', values)
    const {onSubmit} = props;
    if (onSubmit) {
      onSubmit(values);
    }

    form.reset()
  }

  return (
    <form onSubmit={form.handleSubmit(handleSubmit)}>
      <InputField name='title' label='Todo' form={form} />
    </form>
  )
}

TodoForm.propTypes = {
  onSubmit: PropTypes.func
}

export default TodoForm
