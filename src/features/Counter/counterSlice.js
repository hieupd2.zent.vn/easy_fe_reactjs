import { createSlice } from '@reduxjs/toolkit';

const counterSlice = createSlice({
  name: 'counter',
  initialState: 0,
  reducers: {
    increase(state) {
      return state + 1;
    },

    decrease(state) {
      return state - 1;
    },
  },
});

const { actions, reducer } = counterSlice;  // createSlice giúp tự động tạo ra actions và reducer tương ứng
export const { increase, decrease } = actions;  // named export
export default reducer; // default export
