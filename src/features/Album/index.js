import React from 'react';
// import PropTypes from 'prop-types';
import AlbumList from './components/AlbumList';

function AlbumFeature(props) {
  const albumList = [
    {
      id: 1,
      name: 'Tự em Thương Mình',
      thumbnaiUrl: 'https://photo-zmp3.zmdcdn.me/banner/4/0/1/7/4017b68368eb4e49d75b2593e27ac370.jpg',
    },
    {
      id: 2,
      name: "Today Hit's",
      thumbnaiUrl: 'https://photo-zmp3.zmdcdn.me/banner/4/2/b/e/42be71b2dcc5eb23b1fb04fd7ad8bf5a.jpg',
    },
    {
      id: 3,
      name: "Hot hit's Việt Nam",
      thumbnaiUrl: 'https://photo-zmp3.zmdcdn.me/banner/c/b/a/9/cba97d45bb1364798710382164772c80.jpg',
    },
  ];
  return (
    <div>
      <h2>Có thể bạn thích</h2>
      <AlbumList albumList={albumList} />
    </div>
  );
}

// AlbumFeature.propTypes = {};

export default AlbumFeature;
