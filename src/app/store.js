import { configureStore } from '@reduxjs/toolkit';
import counterReducer from '../features/Counter/counterSlice';

// rootReducer bao gồm tất cả các reducer
const rootReducer = {
  count: counterReducer,
};

const store = configureStore({
  reducer: rootReducer,
});

// connect component lên store
export default store;
